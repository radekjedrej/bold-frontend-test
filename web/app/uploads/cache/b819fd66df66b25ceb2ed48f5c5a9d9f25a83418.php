
<section class="billboard d-flex d-flex-wrap">
    <div class="billboar__box d-flex d-align-center">
        <div class="billboard__copy">
            <h2 class="heading-billboard"><?= __( 'Lorem ipsum dolor sit amet consectetur', 'bold_test')  ?></h2>
            <p class="text-intro"><?= __( 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sodales, leo at tempor varius, mauris neque fringilla elit, in tristique augue', 'bold_test')  ?></p>
            <a class="u-btn u-btn--fancy" href="#"><?= __( 'Get Started', 'bold_test')  ?> <span class="text-intro__copy"><?= __( 'Get Started', 'bold_test')  ?></span></a>
        </div>
    </div>
    <div class="billboar__box">
        <div class="billboard__img">
          <?php echo e(\App\sage(\BladeSvg\SvgFactory::class)->svg('illustration', 'billboar__svg')) ?>
        </div>
    </div>

</section>