<footer class="u-footer">
  <div class="wrapper-main">
    <div class="u-footer__inner d-flex d-flex-wrap">
      
      <div class="u-footer__details">
        <a class="u-footer__logo" href="<?php echo e(home_url('/')); ?>"><?php echo e(\App\sage(\BladeSvg\SvgFactory::class)->svg('logo2')) ?></a>
        <p class="u-footer__details__text color-gray line-md line-md"><?= __( 'Get Started', 'bold_test')  ?><?= __( 'Lorem ipsum dolor sit amet consectetur apiliscit est', 'bold_test')  ?></p>
        <ul class="u-footer__social d-flex">
          <li class="u-footer__social__li">
            <a class="u-footer__social__cta" href="#"><?php echo e(\App\sage(\BladeSvg\SvgFactory::class)->svg('facebook', 'u-footer__social__svg')) ?></a>
          </li>
          <li class="u-footer__social__li">
            <a class="u-footer__social__cta" href="#"><?php echo e(\App\sage(\BladeSvg\SvgFactory::class)->svg('twitter', 'u-footer__social__svg')) ?></a>
          </li>
          <li class="u-footer__social__li">
            <a class="u-footer__social__cta" href="#"><?php echo e(\App\sage(\BladeSvg\SvgFactory::class)->svg('instagram', 'u-footer__social__svg')) ?></a>
          </li>
        </ul>
        <p class="color-gray-2"><?= __( '©2020 Bold_test', 'bold_test')  ?></p>
      </div>
      
      <div class="u-footer__products">
        <div class="u-footer__title text-plan"><?= __( 'Product', 'bold_test')  ?></div>
        <ul class="u-footer__ul">
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'Download', 'bold_test')  ?></a></li>
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'Pricing', 'bold_test')  ?></a></li>
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'Locations', 'bold_test')  ?></a></li>
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'Server', 'bold_test')  ?></a></li>
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'Countries', 'bold_test')  ?></a></li>
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'Blog', 'bold_test')  ?></a></li>
        </ul>
      </div>
      
      <div class="u-footer__engage">
        <div class="u-footer__title text-plan"><?= __( 'Engage', 'bold_test')  ?></div>
        <ul class="u-footer__ul">
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'Why', 'bold_test')  ?></a></li>
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'FAQ', 'bold_test')  ?></a></li>
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'Tutorials', 'bold_test')  ?></a></li>
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'About Us', 'bold_test')  ?></a></li>
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'Privacy Policy', 'bold_test')  ?></a></li>
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'Terms of Service', 'bold_test')  ?></a></li>
        </ul>
      </div>
      
      <div class="u-footer__earn">
        <div class="u-footer__title text-plan"><?= __( 'Earn Money', 'bold_test')  ?></div>
        <ul class="u-footer__ul">
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'Affiliate', 'bold_test')  ?></a></li>
          <li class="u-footer__li"><a class="text-gray" href="#"><?= __( 'Become Partner', 'bold_test')  ?></a></li>
        </ul>
      </div>

    </div>
  </div>
</footer>
