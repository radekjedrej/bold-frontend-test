<section class="info">
    <div class="info__inner d-flex d-flex-wrap">
        <div class="info__box d-flex">
            <div class="info__img d-flex d-flex-center">
                    <?php echo e(\App\sage(\BladeSvg\SvgFactory::class)->svg('person')) ?>
            </div>
            <div class="info__content">
                <div class="info__number heading-title"><span class="info__number__count" data-target="90">0</span><span>+</span></div>
                <div class="info__title text-info"><?= __( 'Users', 'bold_test')  ?></div>
            </div>
        </div>
        <div class="info__box d-flex">
            <div class="info__img d-flex d-flex-center">
                    <?php echo e(\App\sage(\BladeSvg\SvgFactory::class)->svg('location')) ?>
            </div>
            <div class="info__content">
                <div class="info__number heading-title"><span class="info__number__count" data-target="30">0</span><span>+</span></div>

                <div class="info__title text-info"><?= __( 'Location', 'bold_test')  ?></div>
            </div>
        </div>
        <div class="info__box d-flex">
            <div class="info__img d-flex d-flex-center">
                    <?php echo e(\App\sage(\BladeSvg\SvgFactory::class)->svg('serwer')) ?>
            </div>
            <div class="info__content">
                <div class="info__number heading-title"><span class="info__number__count" data-target="50">0</span><span>+</span></div>
                <div class="info__title text-info"><?= __( 'Servers', 'bold_test')  ?></div>
            </div>
        </div>
    </div>
</section>
