<header class="u-text">
    <h3 class="u-text-section heading-section"><?= __( 'Choose Your Plan', 'bold_test')  ?></h3>
    <p><?= __( "Let's choose the package that is best for you and explore it happily and cheerfully.", "bold_test")  ?></p>
</header>