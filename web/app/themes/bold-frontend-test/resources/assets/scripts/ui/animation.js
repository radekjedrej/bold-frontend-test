export class Animation {
  constructor() {
    this.iconOne = document.querySelectorAll(".billboard__icon");

    this.init();
  }

  init() {
    this.iconOne.forEach((item) => {
      item.classList.add("billboard__icon--active");
    });
  }
}
