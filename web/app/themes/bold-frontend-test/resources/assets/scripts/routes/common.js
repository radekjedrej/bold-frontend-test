import { Counter } from "../ui/counter";
import { Animation } from "../ui/animation";

export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    new Counter();
    new Animation();
  },
};
