<header class="u-header">
  <div class="wrapper-main">
    <a class="brand" href="{{ home_url('/') }}">@svg('logo')</a>
  </div>
</header>
