<section class="u-card d-flex d-flex-wrap">
    <div class="u-card__box">
        <a href="#" class="u-card__inner d-flex">
            <div class="u-card__top">
                <div class="u-card__image d-flex d-justify-center">
                    <img src="@asset('images/box1.png')">
                </div>
                <h4 class="u-card__title u-text-center text-plan"><?= __( 'Free Plan', 'bold_test')  ?></h4>
                <ul class="u-card__ul">
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Unlimited Bandwitch', 'bold_test')  ?></li>
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Encrypted Connection', 'bold_test')  ?></li>
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'No Traffic Logs', 'bold_test')  ?></li>
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Works on All Devices', 'bold_test')  ?></li>
                </ul>
            </div>
            <div class="u-card__bottom">
                <div class="u-card__price u-text-center heading-title"><?= __( 'FREE', 'bold_test')  ?></div>
                <button class="u-btn" href="#"><?= __( 'Select', 'bold_test')  ?></button>
            </div>
        </a>
    </div>
    <div class="u-card__box">
        <a href="#" class="u-card__inner d-flex">
            <div class="u-card__top">
                <div class="u-card__image d-flex d-justify-center">
                    <img src="@asset('images/box2.png')">
                </div>
                <h4 class="u-card__title u-text-center text-plan"><?= __( 'Standard Plan', 'bold_test')  ?></h4>
                <ul class="u-card__ul">
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Unlimited Bandwitch', 'bold_test')  ?></li>
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Encrypted Connection', 'bold_test')  ?></li>
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Yes Traffic Logs', 'bold_test')  ?></li>
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Works on All Devices', 'bold_test')  ?></li>
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Connect Anyware', 'bold_test')  ?></li>
                </ul>
            </div>
            <div class="u-card__bottom">
                <div class="u-card__price u-text-center heading-title"><?= __( '$9', 'bold_test')  ?><span class="u-card__details"><?= __( ' / mo', 'bold_test')  ?></span></div>
                <button class="u-btn" href="#"><?= __( 'Select', 'bold_test')  ?></button>
            </div>
        </a>
    </div>
    <div class="u-card__box">
        <a href="#" class="u-card__inner d-flex">
            <div class="u-card__top">
                <div class="u-card__image d-flex d-justify-center">
                    <img src="@asset('images/box3.png')">
                </div>
                <h4 class="u-card__title u-text-center text-plan"><?= __( 'Premium Plan', 'bold_test')  ?></h4>
                <ul class="u-card__ul">
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Unlimited Bandwitch', 'bold_test')  ?></li>
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Encrypted Connection', 'bold_test')  ?></li>
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Yes Traffic Logs', 'bold_test')  ?></li>
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Works on All Devices', 'bold_test')  ?></li>
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Connect Anyware', 'bold_test')  ?></li>
                    <li class="u-card__li d-dlex d-align-center text-list color-gray">@svg('check', 'u-card__svg')<?= __( 'Get New Features', 'bold_test')  ?></li>
                </ul>
            </div>
            <div class="u-card__bottom">
                <div class="u-card__price u-text-center heading-title"><?= __( '12', 'bold_test')  ?>$<span class="u-card__details"><?= __( ' / mo', 'bold_test')  ?></span></div>
                <button class="u-btn" href="#"><?= __( 'Select', 'bold_test')  ?></button>
            </div>
        </a>
    </div>
</section>